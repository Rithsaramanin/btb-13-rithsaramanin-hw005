package com.company;

public class Main implements Runnable{
    String n1 = "Hello KSHRD!";
    String n2 = "************************************";
    String n3 = "I will try my best to be here at HRD.";
    String n4 = "------------------------------------";
    String n5 = "Downloading.........";
    String n6 = "Completed 100%!";

    @Override
    public void run() {
        try {
            for (int i=0;i<n1.length();i++){
                System.out.print(n1.charAt(i));
                Thread.sleep(250);
            }
            System.out.println();

            for (int i=0;i<n2.length();i++){
                System.out.print(n2.charAt(i));
                Thread.sleep(225);
            }
            System.out.println();

            for (int i=0;i<n3.length();i++){
                System.out.print(n3.charAt(i));
                Thread.sleep(225);
            }
            System.out.println();

            for (int i=0;i<n4.length();i++){
                System.out.print(n4.charAt(i));
                Thread.sleep(225);
            }
            System.out.println();

            for (int i=0;i<n5.length();i++){
                System.out.print(n5.charAt(i));
                Thread.sleep(250);
            }

//            System.out.println(n6);

            for (int i=0;i<n6.length();i++){
                System.out.println(n6);
                break;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Main runnable = new Main();
        Thread t1 = new Thread(runnable);
        t1.start();
    }
}
